package fr.ul.miage.kelbertp.covid;

import com.google.gson.JsonObject;
import fr.ul.miage.kelbertp.covid.datas.ApiCalls;
import fr.ul.miage.kelbertp.covid.datas.Departement;
import fr.ul.miage.kelbertp.covid.datas.Global;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.logging.Logger;

public class HelloController {

    private static final Logger LOG = Logger.getLogger(HelloController.class.getName());

    private final ApiCalls apiCalls = new ApiCalls();

    @FXML
    public ComboBox<Departement> selectDepartements;
    @FXML
    public LineChart<String, Integer> chartDepDeces;
    @FXML
    public LineChart<String, Integer> chartDepAdmin;
    @FXML
    public LineChart<String, Integer> chartDepHospi;
    @FXML
    public LineChart<String, Integer> chartDepVacc;


    @FXML
    public DatePicker dateFrom;
    @FXML
    public DatePicker dateTo;
    @FXML
    public Button reloadButton;
    @FXML
    public LineChart<String, Integer> chartDepDecesDate;
    @FXML
    public LineChart<String, Integer> chartDepAdminDate;
    @FXML
    public LineChart<String, Integer> chartDepHospiDate;
    @FXML
    public LineChart<String, Integer> chartDepVaccDate;

    @FXML
    public void initialize() {
        try {
            Departement[] departements = apiCalls.getAllDepartements();
            selectDepartements.getItems().addAll(departements);
            selectDepartements.setPromptText("Sélectionnez un département");
        } catch (Exception e) {
            LOG.severe("Erreur lors de l'initialisation : " + e.getMessage());
        }
    }

    @FXML
    public void changeDepartement() {
        try {
            Departement selectedChoice = selectDepartements.getValue();
            Global[] globals = apiCalls.getDatasByDepartement(selectedChoice.nom);
            XYChart.Series<String, Integer> dataDeces = new XYChart.Series<>();
            dataDeces.setName("Décès (" + selectedChoice.nom + ")");

            XYChart.Series<String, Integer> dataAdmin = new XYChart.Series<>();
            dataAdmin.setName("Admissions (" + selectedChoice.nom + ")");

            XYChart.Series<String, Integer> dataHosp = new XYChart.Series<>();
            dataHosp.setName("Hospitalisations (" + selectedChoice.nom + ")");

//            XYChart.Series<String, Integer> dataVacc = new XYChart.Series<>();
//            dataDeces.setName("Vaccinatiosn (" + selectedChoice.nom + ")");


            toSeries(globals, dataDeces, dataAdmin, dataHosp);

            setChart(chartDepDeces, dataDeces);
            setChart(chartDepAdmin, dataAdmin);
            setChart(chartDepHospi, dataHosp);

        } catch (Exception e) {
            LOG.severe("Une erreur s'est produite lors de l'affichage des données\n Erreur :" + e);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Une erreur s'est produite lors de l'affichage des données.");
            alert.setContentText("Veuillez rééssayer svp.");
            alert.show();
        }
    }


    @FXML
    public void changeDate() {
        LocalDate selectedDateFrom = dateFrom.getValue();
        LocalDate selectedDateTo = dateTo.getValue();
        if (selectedDateFrom != null && selectedDateTo != null
                && selectedDateFrom.isBefore(selectedDateTo)
                && selectedDateFrom.isAfter(LocalDate.of(2020, 3, 18))
                && selectedDateTo.isBefore(LocalDate.now())
        ) {
            Global[] globals = apiCalls.getDatasByDate(selectedDateFrom, selectedDateTo);
            XYChart.Series<String, Integer> dataDeces = new XYChart.Series<>();
            dataDeces.setName("Décès");

            XYChart.Series<String, Integer> dataAdmin = new XYChart.Series<>();
            dataAdmin.setName("Admissions");

            XYChart.Series<String, Integer> dataHosp = new XYChart.Series<>();
            dataHosp.setName("Hospitalisations");

//            XYChart.Series<String, Integer> dataVacc = new XYChart.Series<>();
//            dataHosp.setName("Vaccinations");

            toSeries(globals, dataDeces, dataAdmin, dataHosp);

            setChart(chartDepDecesDate, dataDeces);
            setChart(chartDepAdminDate, dataAdmin);
            setChart(chartDepHospiDate, dataHosp);
//            setChart(chartDepDecesDate, dataDeces);
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Date incohérentes");
            alert.setContentText("Veuillez sélectionner des dates correctes. \n " +
                    "La date \"Du...\" doit être supérieure au 18-03-2018.\n " +
                    "La date \"Au...\" doit être inférieure à aujourd'hui.\n");
            alert.show();
        }
    }

    private void toSeries(Global[] globals, XYChart.Series<String, Integer> dataDeces, XYChart.Series<String, Integer> dataAdmin, XYChart.Series<String, Integer> dataHosp) {
        for (Global global : globals) {
            String date = global.date;
            dataDeces.getData().add(new XYChart.Data<>(date, global.incid_dchosp));
            dataAdmin.getData().add(new XYChart.Data<>(date, global.incid_rea));
            dataHosp.getData().add(new XYChart.Data<>(date, global.incid_hosp));
//                dataDeces.getData().add(new XYChart.Data<>(date, global.dchosp));
        }
    }

    private void setChart(LineChart<String, Integer> chart, XYChart.Series<String, Integer> series) {
        chart.getData().clear();
        chart.getData().add(series);
        chart.setVisible(true);
    }


}