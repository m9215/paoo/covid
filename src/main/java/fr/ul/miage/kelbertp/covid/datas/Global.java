package fr.ul.miage.kelbertp.covid.datas;

public class Global {

    public String date;
    public String dep;
    public String reg;
    public String lib_dep;
    public String lib_reg;
    public String hosp;
    public int incid_hosp;
    public String rea;
    public int incid_rea;
    public String rad;
    public int incid_rad;
    public int dchosp;
    public int incid_dchosp;
    public String esms_dc;
    public String dc_tot;
    public String conf;
    public String conf_j1;
    public String pos;
    public String pos_7j;
    public String esms_cas;
    public String tx_pos;
    public String tx_incid;
    public String to;
    public String r;


    @Override
    public String toString() {
        return "Global{" +
                "date='" + date + '\'' +
                ", dep='" + dep + '\'' +
                ", reg='" + reg + '\'' +
                ", lib_dep='" + lib_dep + '\'' +
                ", lib_reg='" + lib_reg + '\'' +
                ", hosp='" + hosp + '\'' +
                ", incid_hosp='" + incid_hosp + '\'' +
                ", rea='" + rea + '\'' +
                ", incid_rea='" + incid_rea + '\'' +
                ", rad='" + rad + '\'' +
                ", incid_rad='" + incid_rad + '\'' +
                ", dchosp='" + dchosp + '\'' +
                ", incid_dchosp='" + incid_dchosp + '\'' +
                ", esms_dc='" + esms_dc + '\'' +
                ", dc_tot='" + dc_tot + '\'' +
                ", conf='" + conf + '\'' +
                ", conf_j1='" + conf_j1 + '\'' +
                ", pos='" + pos + '\'' +
                ", pos_7j='" + pos_7j + '\'' +
                ", esms_cas='" + esms_cas + '\'' +
                ", tx_pos='" + tx_pos + '\'' +
                ", tx_incid='" + tx_incid + '\'' +
                ", to='" + to + '\'' +
                ", r='" + r + '\'' +
                '}' + "\n";
    }
}
