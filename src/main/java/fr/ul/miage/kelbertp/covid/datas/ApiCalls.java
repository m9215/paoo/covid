package fr.ul.miage.kelbertp.covid.datas;

import com.google.gson.Gson;
import javafx.scene.control.Alert;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;
import java.util.regex.Pattern;


public class ApiCalls {

    private static final Logger LOG = Logger.getLogger(ApiCalls.class.getName());

    public Departement[] getAllDepartements() {
        Departement[] departements = new Departement[0];
        try {
            URL oracle = new URL("https://geo.api.gouv.fr/departements?fields=nom,code,codeRegion");
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream(), StandardCharsets.UTF_8));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                Gson g = new Gson();
                departements = g.fromJson(inputLine, Departement[].class);
            }
            in.close();
        } catch (Exception e) {
            showDialogErrorAlert(e);
        }
        return departements;
    }

    public Global[] getDatasByDepartement(String nom) {
        Alert waitAlert = openWaitAlert();
        Global[] globals = new Global[0];
        try {
            URL oracle = new URL("https://coronavirusapifr.herokuapp.com/data/departement/" + nom);
            globals = getDataFromAPICovid(oracle);
        } catch (Exception e) {
            showDialogErrorAlert(e);
        }
        closeWaitAlert(waitAlert);
        return globals;
    }

    public void showDialogErrorAlert(Exception e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erreur");
        alert.setHeaderText("Une erreur est survenue pendant la récupération des données.");
        alert.setContentText("Veuillez rééssayer svp.");
        alert.show();
        LOG.severe("Erreur lors de la recuperation des donnees\n Erreur : " + e.getMessage());
    }

    private Alert openWaitAlert() {
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("Chargement des données");
        a.setHeaderText("Veuillez patienter");
        a.setContentText("Nous chargeons les données demandées.");
        a.show();
        return a;
    }

    public void closeWaitAlert(Alert a) {
        a.close();
    }

    public Global[] getDatasByDate(LocalDate selectedDateFrom, LocalDate selectedDateTo) {
        Alert waitAlert = openWaitAlert();
        ArrayList<Global> globals = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            while (!selectedDateFrom.isEqual(selectedDateTo)) {
                URL oracle = new URL("https://coronavirusapifr.herokuapp.com/data/france-by-date/" + selectedDateFrom.format(formatter));
                globals.add(getDataFromAPICovid(oracle)[0]);
                selectedDateFrom = selectedDateFrom.plusDays(1);
            }
            URL oracle = new URL("https://coronavirusapifr.herokuapp.com/data/france-by-date/" + selectedDateTo.format(formatter));
            globals.add(getDataFromAPICovid(oracle)[0]);
        } catch (Exception e) {
            showDialogErrorAlert(e);
        }
        closeWaitAlert(waitAlert);
        return globals.toArray(new Global[0]);
    }

    public Global[] getDataFromAPICovid(URL oracle) throws Exception {
        Global[] globals = new Global[0];
        URLConnection yc = oracle.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            if (!inputLine.equalsIgnoreCase("No data found")) {
                Gson g = new Gson();
                globals = g.fromJson(inputLine, Global[].class);
            }
        }
        in.close();
        return globals;
    }

}
