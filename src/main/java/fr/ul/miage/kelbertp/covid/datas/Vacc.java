package fr.ul.miage.kelbertp.covid.datas;

public class Vacc {

    private String dep;
    private String jour;
    private String n_dose_1;
    private String n_complet;
    private String n_rappel;
    private String n_com_dose1;
    private String n_com_complet;
    private String n_com_rappel;
    private String couv_dose1;
    private String couv_complet;
    private String couv_rappel;

    public Vacc(String dep, String jour, String n_dose_1, String n_complet, String n_rappel, String n_com_dose1, String n_com_complet, String n_com_rappel, String couv_dose1, String couv_complet, String couv_rappel) {
        this.dep = dep;
        this.jour = jour;
        this.n_dose_1 = n_dose_1;
        this.n_complet = n_complet;
        this.n_rappel = n_rappel;
        this.n_com_dose1 = n_com_dose1;
        this.n_com_complet = n_com_complet;
        this.n_com_rappel = n_com_rappel;
        this.couv_dose1 = couv_dose1;
        this.couv_complet = couv_complet;
        this.couv_rappel = couv_rappel;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }

    public String getN_dose_1() {
        return n_dose_1;
    }

    public void setN_dose_1(String n_dose_1) {
        this.n_dose_1 = n_dose_1;
    }

    public String getN_complet() {
        return n_complet;
    }

    public void setN_complet(String n_complet) {
        this.n_complet = n_complet;
    }

    public String getN_rappel() {
        return n_rappel;
    }

    public void setN_rappel(String n_rappel) {
        this.n_rappel = n_rappel;
    }

    public String getN_com_dose1() {
        return n_com_dose1;
    }

    public void setN_com_dose1(String n_com_dose1) {
        this.n_com_dose1 = n_com_dose1;
    }

    public String getN_com_complet() {
        return n_com_complet;
    }

    public void setN_com_complet(String n_com_complet) {
        this.n_com_complet = n_com_complet;
    }

    public String getN_com_rappel() {
        return n_com_rappel;
    }

    public void setN_com_rappel(String n_com_rappel) {
        this.n_com_rappel = n_com_rappel;
    }

    public String getCouv_dose1() {
        return couv_dose1;
    }

    public void setCouv_dose1(String couv_dose1) {
        this.couv_dose1 = couv_dose1;
    }

    public String getCouv_complet() {
        return couv_complet;
    }

    public void setCouv_complet(String couv_complet) {
        this.couv_complet = couv_complet;
    }

    public String getCouv_rappel() {
        return couv_rappel;
    }

    public void setCouv_rappel(String couv_rappel) {
        this.couv_rappel = couv_rappel;
    }
}
