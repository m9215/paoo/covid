package fr.ul.miage.kelbertp.covid.datas;

public class Departement {

    public String nom;
    public String code;
    public String codeRegion;

    public Departement(String nom, String code, String codeRegion) {
        this.nom = nom;
        this.code = code;
        this.codeRegion = codeRegion;
    }

    @Override
    public String toString() {
        return nom;
    }
}
