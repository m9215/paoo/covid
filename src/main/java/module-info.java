module fr.ul.miage.kelbertp.covid {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires java.logging;
    requires com.google.gson;
    exports fr.ul.miage.kelbertp.covid.datas;
    exports fr.ul.miage.kelbertp.covid;
    opens fr.ul.miage.kelbertp.covid.datas to javafx.fxml;
    opens fr.ul.miage.kelbertp.covid to javafx.graphics;
}