# Librairies utilisées

Toutes les librairies (ou dépendences) sont importées à partir du logiciel [Maven](https://maven.apache.org/).

## Liste

* **JAVA FX** : utilisée pour le design global de l'application. [*Plus d'information...*](https://openjfx.io/)
* **Google GSON** : utilisée pour la déserialisation des données JSON. [*Plus d'informations...*](https://github.com/google/gson)



---



## API

### Liste des départements

*API officielle du gouvernement*

```html
GET https://geo.api.gouv.fr/departements?fields=nom,code,codeRegion
```


| Attributs    | Description                  | Type     |
| :------------- | :----------------------------- | :--------- |
| `code`       | Code (INSEE) du département | `string` |
| `codeRegion` | Code de la région associée | `string` |
| `nom`        | Nom du département          | `string` |

---

### Données du covid

*API non officielle, données issues des données du gouvernement*

```html
GET https://coronavirusapifr.herokuapp.com/data
```

Pour récupérer les données souhaitées, j'ai utilisé une API Github ([CoronavirusAPI-France](https://github.com/florianzemma/CoronavirusAPI-France)), qui me permet de récupérer facilement les données que ce soit en fonction de la région, ou d'un ensemble de dates. Le principal point positif d'avoir utilisé ça plutôt que des fichiers CSV est la qualité des données. En effet, avec les API nous pouvons, sans modifier les fichiers de l'application, avoir accès aux données de la veille.
