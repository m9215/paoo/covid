# Installation

Pour accéder à l'application, il suffit de se placer à la racide du projet, et de lancer le programme nommé `covid` dans le répertoire `covid-1.0.0\bindist\bin`.

**ATTENTION, UNE CONNEXION INTERNET EST REQUISE POUR FAIRE FONCTIONNER L'APPLICATION**
