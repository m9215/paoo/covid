# Manuel d'utilisation

Pour utiliser l'application, rien de plus simple. Après avoir lancé l'application, il suffit de naviguer entre les deux onglets possibles :


### **Par région (par jour)** :

Les données retournées correspondent à l'ensemble des données disponibles pour le département sélectionné. La liste des départements est disponible [ici](https://geo.api.gouv.fr/departements?fields=nom).

---



### **Par intervalle de temps (France entière)** :

Les données retournées correspondent aux données disponibles pour la France entière entre les deux dates sélectionnées.

**En raison de mise à jour de l'API, les données du jour même ne sont pas disponibles.** La date `Du...` doit être inférieur à la date `Au...`. Il n'est pas possible de faire une recherche sur des dates postérieures à la date du jour même.
