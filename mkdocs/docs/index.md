# Projet de M1 MIAGE

*Code écrit par KELBERT Paul*



---



## But du projet

Le but de ce projet est de réaliser une application visant à visualiser des données relatives au COVID-19.

L'application est censée représenter plusieurs indicateurs, comme :

* Les décès
* Les admissions en réanimation
* Les hospitalisations
* Les vaccinations

De plus, il faut que ces données soient accessibles en fonction du département, ou alors en fonction de dates.



---



## Technologies utilisées

Le projet est entirèrement réalisé en [JAVA](https://www.oracle.com/fr/java/technologies/javase/jdk11-archive-downloads.html), version 11.0.10. Mais de nombreuses librairies sont utilisées, voir page [technologies](/covid/mkdocs/site/librairies/).



---



## Contact

Pour toute question, merci de me contacter à l'adresse suivante : paul.kelbert3@etu.univ-lorraine.fr
