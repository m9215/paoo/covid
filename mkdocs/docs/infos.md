# Analyse

### **Comment fonctionne l'application ?**

A chaque changement de sélection des filtres, un appel un API est effectué. Lorsqu'on sélectionne deux dates, plusieurs appels sont effectués (un pour chaque date). 



---



### Interface homme-machine

###### Aspect global

Pour développer cette application, j'ai fait le choix d'un IHM plutôt simple. Peu d'actions utilisateur sont disponibles, car cela évite les erreurs.

###### UX

Lors de l'utilisation de l'application, des fenêtres pop-up peuvent apparaître. Elles permettent à l'utilisateur d'être informé lorsque les données sont en cours de téléchargement, ou alors lorsque des erreurs sont commises par lui *(lors de la sélection des dates par exemple)*.

En cas de problème, un message d'erreur s'affiche car les données ne peuvent pas être récupérées.
