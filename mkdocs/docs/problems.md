# Problèmes rencontrés

Comme dit sur la page d'accueil de la documentation, il fallait que l'application affiche les données vaccinales de la France. Malheureusement, même si j'ai trouvé l'API correspondante (à retrouver [ici](https://public.opendatasoft.com/explore/dataset/covid19-france-stock-vaccin-departement/api/?disjunctive.dep_code&disjunctive.dep_name&disjunctive.reg_code&disjunctive.reg_name)), je n'ai pas réussi à récupérer les données.

J'ai quand même décidé de garder ce système d'API, car je pense qu'il apporte un plus par rapport aux fichiers CSV, qui peuvent rapidement rendre l'application "gourmande" en matière de taille et d'utilisation des ressources du système, car ce type de fichiers peuvent prendre rapidement en taille avec la quantité des données. C'est donc un choix personnel.

De plus, en tant qu'alternant, je n'ai pas eu le temps d'approfondir mes recherches afin de me servir correctement de cette API, par manque de temps (beaucoup de travail en période entreprise).
